package com.lissenberg.beanvalidation;

import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordValidatorTest {
	private static Validator validator;

	@BeforeClass
	public static void init() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	@Test
	public void testPasswords() {
		assertFalse(validator.validate(new PasswordBean(null)).isEmpty());
		assertFalse(validator.validate(new PasswordBean("")).isEmpty());
		assertTrue(validator.validate(new PasswordBean("Password1")).isEmpty());
	}

	static class PasswordBean {
		@Password
		String password;

		PasswordBean(String password) {
			this.password = password;
		}
	}
}
